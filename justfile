STORY_INPUT := "story.ink"
STORY_OUTPUT := "story.json"

default:
  just watch

watch:
  just watch-story {{STORY_INPUT}} {{STORY_OUTPUT}} & just watch-app

build:
  just build-story {{STORY_INPUT}} {{STORY_OUTPUT}} & just build-app

watch-story input output:
  watchexec -e ink just build-story {{input}} {{output}}

build-story input output:
  node ./node_modules/inkjs/dist/inklecate.js -o public/{{output}} {{input}}

watch-app:
  pnpm run dev

build-app: (clean)
  pnpm run build

@clean:
  rm -rf dist
  rm -rf public/story.json