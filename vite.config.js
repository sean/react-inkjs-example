import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { StoryJsonReload } from "./vite-utils";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), StoryJsonReload()],
});
