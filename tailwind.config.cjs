/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.jsx"],
  theme: {
    extend: {
      gridTemplateRows: {
        game: "1fr min-content",
      },
    },
  },
  plugins: [],
};
