import create from "zustand";
import { immer } from "zustand/middleware/immer";

function getMessages(story) {
  let messages = [];
  while (story.canContinue) {
    const next = story.Continue().replace(/\n/g, "");
    messages.push(next);
  }
  return messages;
}

export const useInkStory = create(
  immer((set, get) => {
    function continueStory() {
      const messages = getMessages(get().story);

      set((state) => {
        state.messages.unshift(messages);
        state.tags = get().story.currentTags;
        state.choices = get().story.currentChoices;
      });
    }

    function makeChoice(idx) {
      story.ChooseChoiceIndex(idx);
      continueStory();
    }

    function initializeStory(story) {
      set((state) => {
        state.initialized = true;
        state.story = story;
        state.messages = [getMessages(story)];
        state.tags = story.currentTags;
        state.choices = story.currentChoices;
      });
    }

    function resetStory() {
      get().story.ResetState();
      initializeStory(get().story);
    }

    return {
      initialized: false,
      story: null,
      messages: null,
      tags: null,
      choices: null,
      continueStory,
      makeChoice,
      initializeStory,
      resetStory,
    };
  })
);
