import clsx from "clsx";
import { useCallback } from "react";
import { useInkStory } from "./ink";

export default function Choices() {
  const { choices, makeChoice, resetStory } = useInkStory();

  const selectChoice = useCallback(
    (index) => (e) => {
      e.preventDefault();

      makeChoice(index);
    },
    [makeChoice]
  );

  const reset = useCallback(() => {
    resetStory();
  }, [resetStory]);

  return (
    <ol className={clsx("bg-gray-200", "p-4", "flex", "flex-col", "gap-2")}>
      {choices.map((choice) => (
        <li key={choice.sourcePath + "." + choice.text}>
          <button
            onClick={selectChoice(choice.index)}
            className={clsx(
              "p-2",
              "bg-blue-50",
              "hover:font-bold",
              "focus:font-bold"
            )}
            type="submit"
          >
            {choice.index + 1}. {choice.text}
          </button>
        </li>
      ))}
      <li>
        <button onClick={reset}>Reset</button>
      </li>
    </ol>
  );
}
