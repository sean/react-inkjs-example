import { Story } from "inkjs";
import { useEffect } from "react";
import { useQuery } from "@tanstack/react-query";
import clsx from "clsx";
import Choices from "./Choices";
import Messages from "./Messages";
import { useInkStory } from "./ink";
import Debug from "./Debug";

let story;

function Game() {
  const { data } = useQuery(["storyData"], () =>
    fetch("/story.json").then((res) => res.text())
  );

  const { initializeStory, initialized } = useInkStory();

  useEffect(() => {
    if (!initialized && data) {
      story = new Story(data);
      story.variablesState.GetVariableWithName("DEBUG");
      window.story = story;
      initializeStory(story);
    }
  }, [initializeStory, initialized, data]);

  if (initialized) {
    return (
      <div
        className={clsx(
          "relative",
          "grid",
          "grid-rows-game",
          "mx-auto",
          "aspect-square",
          "max-h-screen",
          "h-full",
          "w-full",
          "max-w-[48rem]"
        )}
      >
        {story?.variablesState.GetVariableWithName("DEBUG")?.value ? (
          <Debug />
        ) : null}
        <Messages />
        <Choices />
      </div>
    );
  }

  return null;
}

export default Game;
