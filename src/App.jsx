import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import Game from "./Game";

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Game title="The Intercept" />
    </QueryClientProvider>
  );
}

export default App;
