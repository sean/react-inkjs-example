import clsx from "clsx";
import { useCallback, useState } from "react";
import { useInkStory } from "./ink";

export default function Debug() {
  const [toggled, setToggled] = useState(false);
  const story = useInkStory((state) => state.story);
  const variables = [...story.variablesState._globalVariables.entries()].map(
    ([key, val]) => [key, val.value]
  );

  const togglePanel = useCallback(() => {
    setToggled(!toggled);
  }, [setToggled, toggled]);

  return toggled ? (
    <div
      className={clsx(
        "absolute",
        "bottom-0",
        "right-0",
        "text-right",
        "bg-red-500",
        "opacity-50",
        "text-white",
        "p-2"
      )}
    >
      {variables.map(([key, val]) => (
        <pre key={key}>
          {key}: {val.toString()}
        </pre>
      ))}
      <button onClick={togglePanel}>hide debug panel</button>
    </div>
  ) : (
    <div className={clsx("absolute", "bottom-0", "right-0", "p-2")}>
      <button onClick={togglePanel}>show debug panel</button>
    </div>
  );
}
