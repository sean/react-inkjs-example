import { useInkStory } from "./ink";

export default function Story() {
  const messages = useInkStory((state) => state.messages);
  const currentMessages = messages[0];

  return (
    <section className="font-serif text-lg p-4">
      {currentMessages.map((text, idx) => {
        return <p key={idx}>{text}</p>;
      })}
    </section>
  );
}
